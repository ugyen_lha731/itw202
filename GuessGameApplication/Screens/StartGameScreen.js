import { StyleSheet, Text, View, TextInput, Alert, Dimensions, useWindowDimensions} from 'react-native'
import React, { useState } from 'react'
import PrimaryButton from '../components/ui/PrimaryButton'
import Title from '../components/ui/Title'
// import { Colors } from '../constants/colors'
import Card from '../components/ui/Card'
import InstructionText from '../components/ui/InstructionText'
import { KeyboardAvoidingView } from 'react-native'
import { ScrollView } from 'react-native'


const StartGameScreen = ({onPickNumber}) => {
    const [EnteredNumber, setEnteredNumber]=useState('');

    const {width, height} = useWindowDimensions();

    function numberInputHandler(enteredText){
        setEnteredNumber(enteredText);

    }
    function resetInputHandler(){
        setEnteredNumber('')
    }
    function confirmInputHandler(){
        const choosenNumber = parseInt(EnteredNumber)

        if(isNaN(choosenNumber) || choosenNumber < 1 || choosenNumber >99){
            Alert.alert('invalid number','number has to be 1 and 99.',
            [{text:'okay', style:'destructive', onPress:resetInputHandler}
        ])
        return;
        }
        
        // console.log(choosenNumber)
        onPickNumber(choosenNumber)

    }
    const marginTopDistance = height< 380 ? 30: 100;
  
  return (
<ScrollView style = {styles.screen}> 
    <KeyboardAvoidingView style = {styles.screen} behavior='position'>
        <View style = {styles.rootContainer}>
            <Title>Guess My Number</Title>
            <Card style={[styles.inputContainer,  {marginTop: marginTopDistance}]}>
                <InstructionText> Enter a Number</InstructionText>
                <TextInput 
                style = {styles.numberInput}
                keyboardType='number-pad'
                maxLength={2}
                autoCorrect={false}
                autoCapitalize='none'
                value={EnteredNumber}
                onChangeText={numberInputHandler}
                />
                <View style = {styles.buttonsContainer}>
                    <View style = {styles.buttonContainer}>
                        <PrimaryButton onPress={resetInputHandler}>Reset</PrimaryButton>
                    </View>
                    <View style = {styles.buttonContainer}>
                        <PrimaryButton onPress={confirmInputHandler}>Confirm</PrimaryButton>
                    </View>
                </View>
            </Card>
            
        </View>
    </KeyboardAvoidingView>
</ScrollView>
  )
}

export default StartGameScreen
// const deviceWidth = Dimensions.get('window').height;

const styles = StyleSheet.create({
    screen:{
        flex:1,
    },
    rootContainer:{
        flex:1,
        // marginTop:deviceWidth< 380 ? 100: 150,
        alignItems:'center',
        marginTop:100

    },
    instructionText:{
        color:'#DDB52F',
        fontSize:24,
    },
    inputContainer:{
        justifyContent:'center',
        alignItems:'center',
        marginTop:100,
        marginHorizontal:24,
        padding: 16,
        backgroundColor:"#72063c",
        borderRadius:8,
        elevation:4,
        // shadowColor:'black',
        // shadowOffset:'width:0, height:2',
        // shadowRadius:5,
        // shadowOpacity:0.2,


    },
    numberInput: {
        height: 50,
        width: 50,
        fontSize: 32,
        borderBottomColor:'#DDB52F',
        borderBottomWidth: 2,
        color: '#DDB52F',
        marginVertical: 8,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    buttonsContainer: {
        flexDirection: 'row'
    },
    buttonContainer: {
        flex: 1,
    }


})