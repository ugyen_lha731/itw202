import { StyleSheet, Text, View } from 'react-native'
const Title = ({children})  => {
  return (
    // <View>
      <Text style = {styles.title}>{children}</Text>
    // </View>
  )
}

export default Title;

const styles = StyleSheet.create({
    title:{
        fontFamily:'open-sans-bold',
        borderWidth:3,
        borderColor:'#fff',
        textAlign:'center',
        // fontWeight:'bold',
        fontSize:22,
        marginTop:5,
        color:'#ffff',
        padding:24,
        maxWidth:'80%',
        width:300
      }
})