import { Pressable, StyleSheet, Text, View } from 'react-native'
import React from 'react'

const PrimaryButton = ({children,onPress}) => {
  return (
  
  <View style = {styles.buttonOuterContainer}>
        <Pressable 
            android_ripple={{color:'yellow'}}
            style = {styles.buttonInnerContainer}
            // style={({pressed})=> pressed ? 
            // [styles.buttonInnerContainer, styles.pressed]:
            // styles.buttonOuterContainer

        // }
        onPress = {onPress}
    
            
            >
            <Text style = {styles.buttonText}>{children}</Text>

        </Pressable>

</View>
  )
}

export default PrimaryButton

const styles = StyleSheet.create({
    buttonOuterContainer:{
        borderRadius: 28,
        margin:4,
        overflow: 'hidden',
    },
    buttonInnerContainer:{
        backgroundColor:'#72063c',
        borderRadius:28,
        paddingVertical:8,
        paddingHorizontal:16,
        elevation:4,
    

    },
    buttonText:{
        color:'white',
        textAlign:'center',

    },
    // pressed:{
    //     opacity: 0.75,
    // }
})