import { LinearGradient } from 'expo-linear-gradient';
import { StatusBar } from 'expo-status-bar';
import { useState } from 'react';
import { ImageBackground, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import GameScreen from './Screens/GameScreen';
import StartGameScreen from './Screens/StartGameScreen';
import GameOverScreen from './Screens/GameOverScreen';
import { useFonts } from 'expo-font';
import AppLoading from 'expo-app-loading';
import Colors from './constants/colors';

export default function App() {
  const [userNumber, setUserNumber] = useState()
  const [gameIsOver, setGameIsOver] = useState(true)
  const [guessRounds, setGuessRounds] = useState(0)

  const [fontsLoaded] = useFonts({
    'open-sans':require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold':require('./assets/fonts/OpenSans-Bold.ttf'),
  })
  if(!fontsLoaded){
    return <AppLoading/>
  }

    function pickedNumberHandler(pickerNumber){
      setUserNumber(pickerNumber);
      setGameIsOver(false);
    }
     function gameOverHandler(numberOfRounds){
       setGameIsOver(true);
       setGuessRounds(numberOfRounds)
     }
     function startNewGameHandler(){
       setUserNumber(null);
       setGuessRounds(0);
     }
    let screen = <StartGameScreen onPickNumber = {pickedNumberHandler}/>


    if(userNumber){
      screen = <GameScreen userNumber = {userNumber} onGameOver = {gameOverHandler}/>
    }
    if(gameIsOver && userNumber){
      screen = <GameOverScreen
                userNumber={userNumber}
                roundsNumber={guessRounds}
                onStartNewGame={startNewGameHandler}
                
                />
    }

  return (
    <LinearGradient colors={[Colors.primary500,Colors.accent500]} style = {styles.container}>
      <ImageBackground
      source={require('./assets/images/background.png')}
      resizeMode='cover'
      style = {styles.image}
      imageStyle={styles.backgroundImage}
      >
         {/* <StartGameScreen/>  */}
         
         </ImageBackground>
        <SafeAreaView style = {styles.SafeAreaView}>
        {screen}
        </SafeAreaView>
        {/* {screen} */}
         
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.accent500
    ,
  },
  image:{
    width:'100%',
    height:'100%',
    flex:1,
    position:'absolute'
  },
  backgroundImage:{
    opacity:0.15,
  },
  SafeAreaView:{
    flex:1
  }
 
});
