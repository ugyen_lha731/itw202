import React, { useState} from 'react';
import {Text, View, StyleSheet, TextInput} from 'react-native';


const TextInputComponent = () => {
    const [text,setText] = useState('');
    return(
        <View>
            <Text>What is your name?</Text>

            <Text>Hi! {text} from Gyelpozhing college of information technology!</Text>
            <TextInput
            placeholder=''
            secureTextEntry = {true}
            onChangeText = {newText => setText (newText)}
            // defaultValue = {text}
            style = {{ backgroundColor:'#e6e6e6' , marginVertical:20}}
            />
             
        </View>
    )
}
export default TextInputComponent;