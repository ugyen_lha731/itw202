import React from 'react'
import { View, StyleSheet } from 'react-native'
import { DrawerItem, DrawerContentScrollView } from '@react-navigation/drawer'
import {
    Avatar,
    Title,
    Caption,
    Paragraph,
    Drawer,
    Text,
    TouchableRipple,
    Switch,
} from 'react-native-paper'

export default function DrawerContent(){
    return (
        <DrawerContentScrollView>
            <View style={styles.drawerContent}>
                <View style={styles.userInfoSection}>
                <Avatar.Image
                size={80}
                source={
                    require("../../assets/stee.jpg")
                }
            />
            <Title style={styles.title}>Sonam Tshering</Title>
            <Caption style={styles.caption}>@Stee</Caption>
            <View style={styles.row}>
                <View style={styles.section}>
                    <Paragraph style={[styles.paragraph, styles.caption]}>
                        0
                    </Paragraph>
                    <Caption style={styles.caption}>Following</Caption>
                </View>
                <View style={styles.section}>
                    <Paragraph style={[styles.paragraph, styles.caption]}>
                        0
                    </Paragraph>
                    <Caption style={styles.caption}>Followers</Caption>
                </View>
            </View>
        </View>
        <Drawer.Section style={styles.drawerSection}>
            <DrawerItem
                label="Setting"
                onPress={() => {}}
            />
        </Drawer.Section>
        <Drawer.Section title="Preferences">
            <TouchableRipple onPress={() => {}}>
                <View style={styles.preference}>
                    <Text>Notifications</Text>
                    <View pointerEvents="none">
                        <Switch value={false}/>
                    </View>
                </View>
            </TouchableRipple>
        </Drawer.Section>
        </View>
    </DrawerContentScrollView>
    )
}
const styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
    },
    userInfoSection: {
        paddingLeft: 20,
    },
    title: {
        marginTop: 20,
        fontWeight: 'bold',
    },
    caption: {
        fontSize: 14,
        lineHeight
        : 14,
    },
    row: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 3,
    },
    drawerSection: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
})