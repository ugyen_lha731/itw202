import React, {useState} from 'react';
import {View, Text, StyleSheet,SafeAreaView, TextInput, TouchableOpacity,ScrollView,StatusBar} from 'react-native';

export default function TODO_12(){
    const[inputText,setInput] = useState(' ');
    const[text,setText] = useState([]);
    const handleText = (inputText) =>{
        setInput(inputText);
    }
    const handleSubmit = () =>{
        setText(current => [...text,inputText])
    }
    const cancel = () => {
        setInput(' ')
    }
    return(
        <SafeAreaView style = {styles.container}>
            <TextInput style = {styles.textinput}
                value = {inputText}
                onChangeText = {handleText} />
                 <View style = {styles.view}>
                     <TouchableOpacity onPress = {handleSubmit}>
                         <Text style = {styles.text1}>ADD</Text>
                     </TouchableOpacity>

                     <TouchableOpacity onPress={cancel}>
                         <Text style = {styles.text2}>CANCEL</Text>
                     </TouchableOpacity>
                 </View>
                 <View>
                     {text.map((inputText)=>
                     <View style = {styles.view1}>
                         <Text style = {styles.text3} key = {inputText}>{inputText}</Text>
                     </View>
                     )}
                 </View>
        </SafeAreaView>
    )
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems: 'center',
        justifyContent:'center',
        paddingTop: StatusBar.currentHeight,
    },
    textinput:{
        borderWidth:1.5,
        borderColor:'black',
        width: 270,
        height: 40

    },
    view:{
        flexDirection: 'row-reverse',
        marginRight:100,
        marginLeft:100,
    },
    text1:{
        color:'blue',
        fontSize:20,
        marginLeft:20
       
        
    },
    text2:{
        color:'red',
        fontSize:20,
       
    },
    view1:{
        padding:10,
        borderColor:'black',
        borderWidth:1,
        backgroundColor:'grey',
        marginRight:200,
        

       
    },
    text3:{
        color:'white',

    },
   

})