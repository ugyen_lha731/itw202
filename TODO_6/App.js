import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.item1}><Text>1</Text></View>
      <View style={styles.item2}><Text>2</Text></View>
      <View style={styles.item3}><Text>3</Text></View>
    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'row',
    backgroundColor: '#fff',
    alignItems: 'flex-start',
    padding: 50,
    paddingRight: 100,
    justifyContent: 'center'
  },
  item1: {
    backgroundColor:'#EE1123',
    height: 230,
    width:60,
    justifyContent:'center',
    alignItems:'center'

  },
  item2: {
    backgroundColor: '#344ACB',
    height: 230,
    width: 150,
    justifyContent:'center',
    alignItems:'center'


  },
  item3: {
    backgroundColor:'#389426',
    height: 230,
    width:8,
    justifyContent:'center',
    alignItems:'center'
  }

});
