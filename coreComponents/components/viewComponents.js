import React from 'react';
import {Text, View, ViewComponent} from 'react-native';
export default function viewComponent() {
    return(
        <View style = {{flex:1}}>
            <View style = {{padding: 100, backgroundColor: 'pink'}}>
                <Text style = {{color: 'red'}}>Text with background color</Text>
            </View>
            <View styles = {{margins: 16}} />
        </View>
    );

}

