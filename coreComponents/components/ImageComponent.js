import React from 'react';
import {View, Image,StyleSheet} from 'react-native';

const ImageComponent = () => {
    return(
        <View>
            <Image
            style={styles.logocontain}
            source = {require('../assets/favicon.png')}
            />
            <Image style ={styles.logoStretch}
            source = {{uri: 'https://about.gitlab.com/images/topics/devops-lifecycle.png'}}
            />
        </View>

    )
}
    export default ImageComponent;

    const styles = StyleSheet.create({
       logocontain:{
           width: 200, height: 100,
           resizeMode:'contain',
       },
       logoStretch :{
           width: 200, height: 100,
           resizeMode: 'stretch',
       },
    });
