import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TextInput, Button} from 'react-native';
import styles from './styles';

export default function App() {
  return (
   /*<View style={{flex:1, justifyContent:'center', alignItems:'center', backgroundColor:'#F5CFF'}}>
      <Text style = {{fontSize: 20, textAlign: 'center', margin:10}}>Welcome to react Native!</Text>
      
   </View>*/
   <View style={styles.Container}>
     <Text style = {styles.Welcome}>
       Welcome to React Native!</Text>
   </View>
  )
}

/*const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    alignItems: 'center',
    justifyContent: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  }
});*/
