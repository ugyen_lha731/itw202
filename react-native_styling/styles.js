import React from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';

const styles = StyleSheet.create({
    Container:{
        flex:1,
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor:'#F5FCFF'

    },
    Welcome:{
        fontSize:20,
        textAlign:'center',
        margin:10
    }
});
export default styles;