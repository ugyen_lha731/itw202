import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style = {styles.box1}></View>
      <View style = {styles.box2}></View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginBottom:50,
    

  },
  box1: {
    backgroundColor: 'red',
    width: 120,
    height:120
  },
  box2: {
    backgroundColor: 'blue',
    width: 120,
    height: 120
    
  }

});
