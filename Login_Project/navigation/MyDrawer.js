import React from "react";
import { View } from "react-native";
import { HomeScreen } from "../src/components/Screens";
import { ProfileScreen } from "../src/components/Screens";
import MyTab from './MyTab'
import {createDrawerNavigator} from '@react-navigation/drawer';
import 'react-native-gesture-handler'
import DrawerContent from "./DrawerContent";
const Drawer = createDrawerNavigator();

export default function MyDrawer(){

    return(
    
    <Drawer.Navigator drawerContent={DrawerContent}>
        <Drawer.Screen name ='Home' component= {MyTab}/>
        
    </Drawer.Navigator>
    )
}