import React from "react";
import {View, StyleSheet} from 'react-native';
import { DrawerItem, DrawerContentScrollView } from "@react-navigation/drawer";
import {
    Avatar,
    Title,
    Caption,
    Paragraph,
    Drawer,
    Text,
    TouchabeRipple,
    Switch,
    TouchableRipple,
} from 'react-native-paper';

export default function DrawerContent(props) {
    return(
        <DrawerContentScrollView>
            <View style={styles.drawerContent}>
                <View style ={styles.userInfoSection}>
                    <Avatar.Image
                    size={80}
                    source={require('../assets/ugyen.jpg')}/>
                <Title style = {styles.title}>Ugyen Lhamo</Title>
                <Caption style = {styles.caption}>Student</Caption>
                <View style = {styles.row}>
                    <Paragraph style = {styles.paragraph}>4</Paragraph>
                    <Caption style = {styles.caption}>Following</Caption>
                </View>
                <View style = {styles.section}>
                    <Paragraph style = {styles.paragraph}>200</Paragraph>
                    <Caption style = {styles.caption}>Followers</Caption>

                </View>

                </View>
            </View>
            <Drawer.Section style = {styles.drawerSection}>
                <DrawerItem
                label="settings"
                onPress = {() => {}}/>
            </Drawer.Section>
            <Drawer.Section title="Preferences">
                <TouchableRipple onPress={() =>{}}>
                    <View style = {styles.preference}>
                        <Text>Notifications</Text>
                        <View pointerEvents="none">
                            <Switch value = {true}/>
                        </View>
                    </View>
                </TouchableRipple>
            </Drawer.Section>
        
        </DrawerContentScrollView>
        
    )
}
const styles = StyleSheet.create({
    drawerContent:{
        flex:1,
    },
    userInfoSection:{
        paddingLeft:20,
    },
    title:{
        marginLeft:20,
        fontWeight:'bold'
    },
    caption:{
        fontSize:14,
        lineHeight: 14,

    },
    row:{
        marginTop:20,
        flexDirection:'row',
        alignItems:'center',
    },
    section:{
        flexDirection:'row',
        alignItems:'center',
        marginRight:15
    },
    paragraph:{
        fontWeight:'bold',
        marginRight:3,
    },
    drawerSection:{
        marginTop:15,
    },
    preference:{
       flexDirection:'row',
       justifyContent:'space-between',
       paddingVertical:12,
       paddingHorizontal:16, 
    },  

})