import React from "react";
import { Image, View } from "react-native"
import {HomeScreen} from "../src/components/Screens";
import { ProfileScreen } from "../src/components/Screens";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import 'react-native-gesture-handler'

const Tab = createBottomTabNavigator();

export default function MyTab(){
    return(
      <Tab.Navigator screenOptions={{headerShown:false}}>
        <Tab.Screen 
        name='Home' 
        component ={HomeScreen}
        options={{
          tabBarIcon:({size}) => {
            return(
              <Image
              style={{width:size, height: size}}
              source={
                require('../assets/home.png')
              }
              />
            );
          },
        }}
        />
        <Tab.Screen 
        name = 'Profile' 
        component={ProfileScreen}
        options={{
          tabBarIcon:({size})=>{
            return(
              <Image
              style = {{width:size, height:size}}
              source={require('../assets/settings.png')}/>
            )
          }
        }}
        
        />
      </Tab.Navigator>
    )
  
  }