import {View, Text} from 'react-native'
import React from 'react'
import {StartScreen, LoginScreen, RegisterScreen,ResetPasswordScreen, HomeScreen,AuthLoadingScreen}
from '../src/components/Screens'
import {createNativeStackNavigator} from '@react-navigation/native-stack'
import 'react-native-gesture-handler'
import MyDrawer from './MyDrawer'

const Stack = createNativeStackNavigator();

 export default function MyStack(){
     return(
         <Stack.Navigator initialRouteName = 'AuthLoadingScreen' screenOptions={{headerShown:false}}>
        <Stack.Screen name='AuthLoadingScreen' component={AuthLoadingScreen}/>
         <Stack.Screen name = 'StartScreen' component = {StartScreen}/>
          <Stack.Screen name='LoginScreen' component={LoginScreen}/>
          <Stack.Screen name='RegisterScreen' component={RegisterScreen}/>
          <Stack.Screen name='ResetPasswordScreen' component={ResetPasswordScreen}/>
          <Stack.Screen name = 'HomeScreen' component = {MyDrawer}/>
          </Stack.Navigator>
      

 
     )
 }