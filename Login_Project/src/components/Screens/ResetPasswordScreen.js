import React, {useState} from "react";
import {Text,View, StyleSheet} from 'react-native';
import Button from "../Button";
import Header from "../Header";
import Background from "../Background";
import Logo from "../Logo";
import TextInput from "../TextInput";
import { emailValidator } from "../../core/helpers/emailValidator";
import BackButton from "../BackButton";


export default function ResetPasswordScreen({navigation}){
    const [email, setEmail] = useState({value:'', error:''})

    const onSubmitPressed = () => {
        const emailError = emailValidator(email.value);
        if(emailError){
            setEmail({...email, error:emailError});
        }
    }
    return(
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>Restore Password</Header>
            <TextInput
            label='Email'
            value={email.value}
            error={email.error}
            errorText = {email.error}
            onChangeText = {(text) => setEmail({ value:text, error:''})}
            description = "You will receive with password reset link."
            />
            <Button mode ='contained' onPress = {onSubmitPressed}>Send Instructions</Button>
        </Background>
    )
}

