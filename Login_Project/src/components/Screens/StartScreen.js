import React from "react";
import { View, StyleSheet} from 'react-native'
import Header from "../Header";
import Paragraph from "../Paragraph";
import Button from "../Button";
import Background from "../Background";
import Logo from "../Logo";


export default function StartScreen({navigation}){
    return(
        <Background>
            <Logo/>
            <Header>Login Template</Header>
            <Paragraph>
                The easiest way to start with your amazing application.
            </Paragraph>
            <Button
            mode='outlined' onPress = {() => {
                navigation.navigate('LoginScreen')
            }}
            >
                Login
            
            </Button>

            <Button 
            mode = 'contained'
            onPress = {() => {
                navigation.navigate('RegisterScreen')
            }}
            >
                Sign Up
                </Button>
        
        </Background>
    )
}

