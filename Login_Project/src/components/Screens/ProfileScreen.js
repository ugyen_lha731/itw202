import React from "react";
import Header from "../Header";
import Background from "../Background";

export default function ProfileScreen(){
    return(
        <Background>
            <Header>Profile</Header>
        </Background>
    )
}