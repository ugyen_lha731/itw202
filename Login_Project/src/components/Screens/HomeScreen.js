import React from "react";
import Header from "../Header";
import Background from "../Background";
import Button from "../Button";
import { logoutUser } from "../../api/auth-api";

export default function HomeScreen(){
    return(
        <Background>
            <Header>Home</Header>
            <Button 
                mode = 'contained' 
                onPress = {() =>{
                    logoutUser();
                    }}
                >
                    Logout
                    </Button>
        </Background>
    )
}