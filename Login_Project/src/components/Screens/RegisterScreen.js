import React, {useState} from 'react';
import {Text,View, StyleSheet, Touchable} from 'react-native'
import Button from '../Button';
import Header from '../Header';
import Paragraph from '../Paragraph';
import Background from '../Background';
import Logo from '../Logo';
import TextInput from '../TextInput';
import { emailValidator } from '../../core/helpers/emailValidator';
import { passwordValidator } from '../../core/helpers/passwordValidator';
import { nameValidator } from '../../core/helpers/nameValidator';
import BackButton from '../BackButton';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { theme } from '../../core/theme';
import {signUpUser} from '../../api/auth-api';

export default function RegisterScreen({navigation}){
    const [email, setEmail] = useState({value:'',error:''})
    const [password,setPassword] = useState({value:'', error:''})
    const [name,setName] = useState({value:"", error:""})
    const [loading, setLoading] = useState();
 
    const onSignUpPressed = async() =>{
        const nameError = nameValidator(name.value);
        const emailError = emailValidator(email.value);
        const passwordError = passwordValidator(password.value);
        if(emailError || passwordError || nameError){
            setName({...name, error:nameError});
            setEmail({...email,error:emailError});
            setPassword({...password, error: passwordError});

        }
        setLoading(true)
        const response = await signUpUser({
            name: name.value,
            email:email.value,
            password: password.value
        })
        if(response.error){
            alert(response.error)
        }
        else{
            console.log(response.user.displayName)
            alert(response.user.displayName)
        }
        setLoading(false)
    }
    return(
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>Create Account</Header>
            <TextInput
            value={name.value}
            error={name.error}
            errorText = {name.error} 
            onChangeText={(text) => setName({value:text, error:''})}
            label='Name'
            />
            <TextInput
            label='Email'
            value={email.value}
            error = {email.error}
            errorText={email.error}
            onChangeText = {(text)=> setEmail({value:text, error:''})}
            />
            <TextInput
            value={password.value}
            error={password.error}
            errorText = {password.error} 
            onChangeText={(text)=> setPassword({value:text,error:''})}
            label = 'Password'
            secureTextEntry/>
            <Button loading ={loading}  mode='contained' onPress = {onSignUpPressed}>Sign Up</Button>
            <View style={styles.row}>
                <Text>Already have an account? </Text>
                <TouchableOpacity onPress={() => navigation.replace("LoginScreen")}>
                    <Text style={styles.link}>Login</Text>
                </TouchableOpacity>
            </View>
    
        </Background>
    )

}
const styles = StyleSheet.create({
    row:{
        flexDirection:'row',
        marginTop:4,
    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary,
    }
})


