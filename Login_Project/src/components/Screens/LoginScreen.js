import React, {useState} from "react";
import {Text,View, StyleSheet, TouchableOpacity} from 'react-native';
import Button from "../Button";
import Header from "../Header";
import Paragraph from "../Paragraph";
import Background from "../Background";
import Logo from "../Logo";
import TextInput from "../TextInput";
import { emailValidator } from "../../core/helpers/emailValidator";
import { passwordValidator } from "../../core/helpers/passwordValidator";
import BackButton from "../BackButton";
import { theme } from "../../core/theme";
import { loginUser } from "../../api/auth-api";


export default function LoginScreen({navigation}){
    const [email, setEmail] = useState({value:'', error:''})
    const [password, setPassword] = useState({ value:'', error:''})
    const [loading, setLoading] = useState();

    const onLoginPressed = async() =>{
        const emailError = emailValidator(email.value);
        const passwordError = passwordValidator(password.value);
        if(emailError || passwordError){
            setEmail({...email,error:emailError});
            setPassword({...password, error: passwordError});
        }
        setLoading(true)
        const response = await loginUser({
            email:email.value,
            pasword:password.value,
        });
        if(response.error){
            alert(response.error);

        // }else{
        //     alert(response.user.displayName);
        }
        setLoading(false)
        
    }
    return(
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>Welcome</Header>
            <TextInput 
            label ='Email'
            // value = 'test'
            // onChangeText= {() => {}}
            value={email.value}
            error={email.error}
            errorText={email.error}
            onChangeText={(text) => setEmail({value:text,error:''})}
            />
            <TextInput 
            value = {password.value}
            error = {password.error}
            errorText= {password.error}
            onChangeText = {(text) => setPassword({ value: text, error:''})}
            label='Password'
            secureTextEntry/>

            <View style = {styles.forgotPassword}>
               <TouchableOpacity
               onPress={() => navigation.navigate('ResetPasswordScreen')}
               >
                   <Text style ={styles.forgot}>forgot your password? </Text>
                </TouchableOpacity> 
            </View>

            <Button loading ={loading} mode='contained' onPress = {onLoginPressed}>Login</Button>
            <View style={styles.row}>
                <Text>Don't have an account? </Text>
                <TouchableOpacity onPress={() => navigation.replace("RegisterScreen")}>
                    <Text style={styles.link}>Sign Up</Text>
                </TouchableOpacity>
            </View>
        </Background>
    )
}
const styles = StyleSheet.create({
    row:{
        flexDirection:'row',
        marginTop:4,
    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary,
    },
    forgotPassword:{
        width:'100%',
        alignItems: 'flex-end',
        marginBottom: 24,
    },
    forgot:{
        fontSize: 13,
        color: theme.colors.secondary,
    }
})