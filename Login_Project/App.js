import { Provider} from 'react-native-paper';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
//import { StyleSheet, Text, View, Image} from 'react-native';
import { theme } from './src/core/theme';
import Button from './src/components/Button';
import TextInput from './src/components/TextInput';
import Header from './src/components/Header';
import {NavigationContainer} from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import firebase from 'firebase/app'
import { firebaseConfig } from './src/core/config';

import {
  StartScreen,
  LoginScreen,
  RegisterScreen,
  ResetPasswordScreen,
  ProfileScreen,
  HomeScreen
} from './src/components/Screens';
import 'react-native-gesture-handler'
import MyStack from './navigation/MyStack'



if(!firebase.apps.length){
  firebase.initializeApp(firebaseConfig);
}

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <Provider>
      <NavigationContainer>

        <MyStack/>
      </NavigationContainer>
    </Provider>

  );
}
 