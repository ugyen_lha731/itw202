import react from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';

const ImageComponent = () => {
    return(
        <View>
            <Image
            style = {styles.pic1}
            source = {{uri:'https://picsum.photos/100/100'}}
            />
            <Image
            style = {styles.pic2}
            source = {require('../assets/reactnative.png')}
            />
        </View>
    )
}
export default ImageComponent;

const styles = StyleSheet.create({
    pic1:{
        width: 100, height: 100,
        marginBottom : 50
    },
    pic2:{
        width: 100, height : 100
    }

})