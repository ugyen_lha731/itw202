import React from "react";
import {TouchableOpacity, Text,View, StyleSheet} from 'react-native'
import { COLORS } from "./constants/Colors";

function CustomComponents() {
    return(
        <View style = {styles.container}>
            <TouchableOpacity style = {styles.row}>
                <Text style = {styles.text}>Theme</Text>
            </TouchableOpacity>
            <TouchableOpacity style = {styels.row}>
                <Text>React Native Basics</Text>
            </TouchableOpacity>
            <TouchableOpacity style = {styles.row}>
                <Text>React Native by Example</Text>
            </TouchableOpacity>
        </View>
    )
}
export default CustomComponents;
const styles = StyleSheet.create({
    container:{
        marginTop:30
    },
    row:{
        paddingHorizontal:20,
        paddingVertical:16,
        justifyContent:'space-between',
        alignItems:'center',
        flexDirection:'row',
        backgroundColor: COLORS.white,
    },
    title:{
        color: COLORS.text,
        fontSize: 16
    }
})