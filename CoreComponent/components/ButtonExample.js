import React, { useState } from 'react';
import { View, Text, Button} from 'react-native';
const ButtonExample = () => {
    const [count, setCount] = useState(0);

return(
    <View style = {{padding:100}}>
        <Text>You Clicked {count} times</Text>
        <Button
        onPress={() => setCount(count + 1)}
        title = 'Count'/>

    </View>
);
};
export default ButtonExample;