import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default function MyText() {
    return(
        <View style={styles.textcont}>
            <Text>The <Text style = {styles.text2}>quick brown fox</Text> jumps over the {'\n'} lazy dog</Text>
        </View>
    );
}
const styles = StyleSheet.create({
    textcont: {
        flex: 1,
        fontSize: 16,
        justifyContent:'flex-end',
        marginBottom: 150

    },
    text2:{
        fontWeight: 'bold',
        fontSize: 16
    },
});
