import { ScrollView, StyleSheet, Text, View,TextInput  } from 'react-native'
import React, { useState } from 'react'
import {colors} from '../../constants/colors'
import ImagePicker from './ImagePicker';
import LocationPicker from './LocationPicker';


function PlaceForm(){
  const[enteredTitle, setEneteredTitle] = useState('');

  function changeTitleHandler(enteredText){
    setEneteredTitle(enteredText);
  }
  return (
    <ScrollView style = {styles.form}>
      <View>
        <Text style = {styles.label1}>Title</Text>
        <TextInput 
        style = {styles.input}
        onChangeText = {changeTitleHandler}
        value = {enteredTitle}
        />
      </View>
      <ImagePicker/>
      <LocationPicker/>

    </ScrollView>
  )
}

export default PlaceForm

const styles = StyleSheet.create({
  form:{
    flex:1,
    padding:24,
  },
  label1:{
    fontWeight: 'bold',
    marginBottom:4,
    color:colors.primary100,
  },
  input:{
    marginVertical:8,
    paddingHorizontal:4,
    paddingVertical:8,
    fontSize:16,
    borderBottomColor:colors.primary700,
    borderBottomWidth:2,
    backgroundColor:colors.primary100,
  }
})