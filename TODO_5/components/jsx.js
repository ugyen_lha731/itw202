import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Jsx = () =>{
    const name='Ugyen Lhamo';
    return(
        <View>
            <Text style= {styles.name}>Getting started with the react native!</Text>
            <Text style = {styles.Name2}>My name is {name}</Text>
        </View>
    )
};
export default Jsx;
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    name: {
    fontSize: 45

    },
    Name2: {
        fontSize:20
    }
    
  });
